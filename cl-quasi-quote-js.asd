;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;;
;;; Copyright (c) 2008 by the authors.
;;;
;;; See LICENCE for details.

(cl:in-package :cl-user)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (asdf:find-system :cl-quasi-quote))

(in-package #:cl-quasi-quote-system)

(define-qq-subsystem :cl-quasi-quote-js
  :version "1.0"
  :description "Quasi quote transformations for emitting JavaScript"
  :setup-readtable-function "cl-quasi-quote-js::setup-readtable"
  :depends-on (:cl-ppcre
               :cl-quasi-quote
               :cl-walker
               )
  :components
  ((:module "src"
            :components
            ((:module "js"
                      :components
                      ((:file "package")
                       (:file "escaping" :depends-on ("package"))
                       (:file "repositories" :depends-on ("package"))
                       (:file "ast" :depends-on ("package" "repositories"))
                       (:file "syntax" :depends-on ("package" "ast" "repositories"))
                       (:file "transform" :depends-on ("package" "escaping" "syntax" "ast"))
                       (:file "js-utils" :depends-on ("transform"))))))))

(define-qq-system-connection :cl-quasi-quote-xml-and-cl-quasi-quote-js
  :requires (:cl-quasi-quote-xml :cl-quasi-quote-js)
  :setup-readtable-function "cl-quasi-quote::setup-readtable"
  :components
  ((:module "src"
            :components
            ((:module "js"
                      :components
                      ((:file "xml-integration")))))))
